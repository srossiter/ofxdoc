ofxdoc
======

There is a [live version of the app](http://ofxdoc.herokuapp.com/ "ofxdoc").

Simple OrcaFlex model documentation web app on CherryPy web framework with jinja2 templating.

## Prerequisites

Tested with Python 2.7 for windows will need at least 2.4 because without the collections module what's the point?  
[pyYAML](https://bitbucket.org/xi/pyyaml "pyYAML")  
[CherryPy](http://www.cherrypy.org/ "CherryPy")  
[Jinja2](http://jinja.pocoo.org/docs/ "Jinja2")  

## Running App

From the ofxdoc folder, simply:

`python app.py`

Go to [localhost:6722](http://localhost:6722 "localhost:6722") in a browser - any normal browser should work, even looks decent on phone and tablets.

## Templating

The real magic is going on in model.html which is essentially just yaml->html but with some knowledge of what OrcaFlex text data files look like. 

## Examples

Taken from the [Orcina OrcaFlex examples](http://www.orcina.com/SoftwareProducts/OrcaFlex/Examples/index.php). 

## Static content

Ummmm... do not look in there, it is ugly. Uses the JqueryUI accordion to make it look pretty and is standing on the shoulders of HTML5 boilerplate.  

