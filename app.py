import yaml, re, os, collections
import cherrypy as http
from jinja2 import Environment, FileSystemLoader
# CherryPy and Jinja config stuff
env = Environment(loader=FileSystemLoader('templates'))
curDir = os.path.realpath(__file__)
config = {'/static': {
        'tools.staticdir.on' : True,
        'tools.staticdir.dir' : os.path.normpath(os.path.split(curDir)[0] + "/static/")},
        '/': {}}

class App:
    ''' Main web application class.'''
    
    @http.expose
    def index(self):
        examples = os.listdir('examples') # list all the example files
        tmpl = env.get_template('base.html') # render the base template
        return tmpl.render(examples=examples)
    
    @http.expose
    def upload(self, myFile=None, example_choice=None):
        ''' This method is called asynchronously from the index page.
        It takes the uploaded file or choice of example and returns the html rendering'''
        if example_choice:
            return ofx2html(open("examples/{}".format(example_choice), 'rb').read())
        elif myFile:
            # check it is ain't markup or yet another
            if myFile.filename.split('.')[-1] not in ['yml', 'yaml']:
                return "<em>Not a yml file I don't fink!</em>"
            else:
                return ofx2html(myFile.file.read())
        else:
            return "<em>Choose a file to upload or an example!</em>"
        
def ofx2html(yml_file):
    ''' Converts a yaml string to html. '''
    y=yaml.load(yml_file) # Load the yaml markedup or no
    
    # create filter and test functions for the template    
    # Tests:    
    def is_dict(d): return type(d)==dict
    env.tests['a_dict'] = is_dict    
    def is_list(l): return type(l)==list
    env.tests['a_list'] = is_list    
    def is_value(l): return (type(l) in [str, int, float, bool]) or (l is None)
    env.tests['a_value'] = is_value    
    def is_vector(l): return all([is_value(elem) for elem in l])
    env.tests['a_vector'] = is_vector
    def is_table(l): return all([type(elem)==list for elem in l])
    env.tests['a_table'] = is_table    
    def is_named(d): return d.get('Name',False)
    env.tests['named'] = is_named    
    
    # Filters:
    def pop_name(d): return d.pop('Name',False)
    env.filters['popname'] = pop_name
    first_cap_re = re.compile('(.)([A-Z][a-z]+)')
    all_cap_re = re.compile('([a-z0-9])([A-Z])')
    def un_camelcase(name):
        # Convert the variable names from CamelCase to Variable Name. Struggles with RAOs - could be smarter.
        if type(name)==str:
            s1 = first_cap_re.sub(r'\1 \2', name)
            return all_cap_re.sub(r'\1 \2', s1)
        else:
            return name
    env.filters['uncamel'] = un_camelcase
    def split_commas(s): return s.split(',')
    env.filters['make_headers'] = split_commas
    
    # put the general and environment at the top as in the model browser - then order the sections alphabetically
    ofxItems = [('General', y.pop('General')), ('Environment', y.pop('Environment'))] + sorted(y.items(), key=lambda t: t[0])
    # turn the pyyaml object into and ordered dict. Any excuse to import collections...
    oYml = collections.OrderedDict(ofxItems)
    
    # load the web template
    t_web = env.get_template('model.html')
    
    # render the model html
    web_html = t_web.render(yml=oYml)
    return web_html
    
if __name__ == "__main__":
    http.tree.mount(App(), "/", config=config)
    # App will listen on all IPv4 network interfaces. Expect Windows to have some beef with that.
    http.config.update({'server.socket_host': '0.0.0.0',}) 
    http.config.update({'server.socket_port': int(os.environ.get('PORT', '6722')),}) # It's Orca on a keypad init?
    http.engine.start()
    http.engine.block()