/** External Links **/
$(document).ready(function() {
function externalLinks() { if (!document.getElementsByTagName) return; var anchors = document.getElementsByTagName("a"); for (var i=0; i<anchors.length; i++) { var anchor = anchors[i]; if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external") anchor.target = "_blank"; } } window.onload = externalLinks;

/** Responsive Menu **/

$(function() {
	$("<select />").appendTo(".menu");
	$("<option />", { "selected": "selected", "value" : "", "text" : "Go to..." }).appendTo(".menu select");
	$("#headerright a").each(function() { var el = $(this); $("<option />", { "value" : el.attr("href"), "text" : el.text() }).appendTo(".menu select"); });
	$("#headerright select").change(function() { window.location = $(this).find("option:selected").val(); });
	
	
});

var options = { target: '#model_data',                 
                success:    function() {
    $( "#accordion" )
      .accordion({
        header: "> div > h4",
        heightStyle: "content",
        collapsible: true
      })
      .sortable({
        axis: "y",
        handle: "h4",
        stop: function( event, ui ) {

          ui.item.children( "h4" ).triggerHandler( "focusout" );
        }
      });
  $("#progress").html("")

  
  },
  beforeSerialize: function($form, options) {
      $("#progress").html('<img alt="" src="static/images/loading.gif" />')
    // return false to cancel submit                  
    }
  };

$(document).ready(function() { 
  
            $('#yml_file').ajaxForm(options); 
        }); 
});